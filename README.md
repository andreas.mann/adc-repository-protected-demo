# adc-repository-protected-demo

Minimum steps for a protected adc repository (demo installation of the turnkey service and adc-middleware with ubuntu 22.04 LTS) accessible with certificates from custom ca. This has been tested on Fedora 36 with quemu and ubuntu 22.04 LTS.

![Alt text](container-overview.png?raw=true "Overview - container and host ports")

## 1) Prepare a virtual machine 
Use e.g. virtual machine manager (2 vcpu/ 4 gb memory/ 32 gb disc).

## 2) Install Operating System ubuntu 22.04 LTS
Add all tools required, e.g. nginx, curl, vim, ss, ..

Add hosts entries for services and assign ip of network adapter
sudo vim /etc/hosts
```
192.168.122.217	repository1
192.168.122.217	server1

```

## 3) Prepare /var/data folder and copy ca folder to /var/data/ca
- ca files were created based on https://jamielinux.com/docs/openssl-certificate-authority
- sudo mkdir /usr/local/share/ca-certificates/extra
- copy ca.crt and intermediate.crt to that location (need to be in pem format with .crt)
- sudo update-ca-certificates, this creates the trust between nginx and the custom ca on the host system
- when you git clone the software and use version specific folder names, create symbolic links
 with the software name, these names are used in the following steps: e.g. /var/data/adc-middleware 
 instead of /var/data/adc-middleware-1.9.5
 
## 4) turnkey-service
- git clone https://github.com/sfu-ireceptor/turnkey-service-php to /var/data
- in the scripts folder: edit docker-compose.yml
external ports are changed:
```
	     ports:
            - ${API_PORT:-8180}:80
            - ${API_PORT_HTTPS:-8143}:443
```
- install turnkey 
- install test study data

## 5) nginx ssl
- use the certificates in /var/data/ca/intermediate/certs and /var/data/ca/intermediate/private 
- adccert.cert.pem contains the server certificate and adccert.key.pem the private key 	
- prepare nginx certificates in /etc/nginx/ssl/, see config below
- the final certificate file adccert.crt contains also the intermediate and root ca certificate and is found in ca/intermediate/certs
 
## 6) nginx config for the services
- cd to /etc/sites-available
- vim vhost-repository1.conf
```
	server {
	    listen       443 ssl http2;

	    ssl_certificate /etc/nginx/ssl/adccert.crt;
	    ssl_certificate_key /etc/nginx/ssl/adccert.key;

	    server_name  repository1;

	    index index.html;

	    location / {
		root   /usr/share/nginx/html;
		index  index.html;
		try_files $uri $uri/ /index.html;
	    }

	   location /airr/ {
		proxy_set_header Host $host;
		proxy_pass http://localhost:8180/airr/;
	   }

	    # iR+
	    location /irplus/ {
		    proxy_pass http://localhost:8180/irplus/;
	    }

	    #error_page  404              /404.html;

	    # redirect server error pages to the static page /50x.html
	    #
	    error_page   500 502 503 504  /50x.html;

	    location = /50x.html {
		root   /usr/share/nginx/html;
	    }
	}

```
- create symb. link in sites-enabled
- do the same with vhost-server1.conf
```
server {

    listen       443 ssl;

    ssl_certificate /etc/nginx/ssl/adccert.crt;
    ssl_certificate_key /etc/nginx/ssl/adccert.key;


    server_name  server1;

          location /auth {
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;

            proxy_set_header Host server1; # VERY IMPORTANT: must update with server's hostname/IP and ports that is exposed to the public

            proxy_pass http://localhost:8081/auth;
            include cors.conf;
            add_header 'Access-Control-Allow-Origin' '*';

        }
   # OpenAPI
        location /middleware/ {
            proxy_pass http://localhost:9443/middleware/;
        }

    # Adc endpoints (/ /info /public_fields /synchronize)
        location /airr/ {

            proxy_pass http://localhost:9443/airr/;
            #include cors.conf;
        }

   # iR+
        location /irplus/ {
            proxy_pass http://localhost:9443/irplus/;
        }

   # Resource controller
        location /resource/ {
            proxy_pass http://localhost:9443/resource/;
        }

   # Authz controllers
        location /authz/ {
            proxy_pass http://localhost:9443/authz/;
        }
        
   # ------------------------------- 
   # access acl-dashboard
        location /acl-dashboard/ {
            #proxy_set_header Host legolas;
            proxy_set_header Host $http_host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            #proxy_pass_header Content-Type;
            #proxy_hide_header Content-Type;
            #try_files $uri $uri/ /index.html;
            #proxy_pass http://localhost:9091/acl-dashboard/;

            proxy_pass http://localhost:9091/;
   }

   # access keycloak extension api
        location /kclext/ {
                proxy_set_header Host $http_host;
                proxy_pass http://localhost:9000/;
   }
}
```

- test config with sudo nginx -t and verify test data access with curl, postman or browser
- in a production environment this path to the repository endpoint should protected from the public

## 7) adc-middleware
- git clone https://github.com/ireceptorplus-inesctec/adc-middleware.git
- copy deplyoment folder to working folder: cp -R deployment/ working
- cd to working/config/middleware && vim deplyoment.properties
edit these entries 
```
adc.resourceServerUrl=https://repository1/airr/v1
app.resourceAllowedOrigins=*
..
uma.wellKnownUrl=https://server1/auth/realms/master/.well-known/uma2-configuration
uma.adminUrl=https://server1/auth/admin/realms/master

uma.clientSecret=[value comes later, when keyloak is running]
uma.keycloakExtensionApiUri=https://server1/kclext
```
- copy ca.cert.pem.crt, intermediate.cert.pem.crt to working/config folder
- the config folder will be mapped into containers to make the custom ca files recognized
- in working folder edit docker-compose.yml:

```
	
	(1) fully remove services: entries for containers
	-- repository-db
	-- repository
	(2) services: 
	middleware:
	volumes:
	..
	  - ./config:/config <-- contains certificates for import 
	environmant:
	  - UMA_CLIENT_SECRET=vyXtgLBOKDGP7cXFrrqQtC2bHK23gvJi	<-- value will be added later
    	ports:
      	  - 9443:80 <--- mw external port 9443
      	  
      	(3) keycloak container
      	keycloak:
        volumes:
          - ./config:/config <-- contains certificates for import 
	ports:	
          - 8081:8080 <-- 8081 is external port
      	
      	(4)keycloakextensionapi:
	image: irpinesctec/keycloak_extension_api:0.7.0	<-- image version!
	volumes:
	   - ./config:/config <-- contains certificates for import 
	environment:
	 REQUESTS_CA_BUNDLE: /etc/ssl/certs/ca-certificates.crt <-- 
	ports:	
          - 9000:5000 <-- 9000 is external port      	
```

- after editing the file: in working folder sudo docker-compose up	

## 8) keycloak 1st use
 - follow steps: https://github.com/ireceptorplus-inesctec/adc-middleware#keycloak-deployment 
 - step 3 client adc-middleware already exists with secret: vyXtgLBOKDGP7cXFrrqQtC2bHK23gvJi
 - add client secret to file working/docker-compose.yml as envirnment variable value
 - same for working/config/middleware/deployment.properties 
 
## 9) upate middleware deplyoment.properties
 - working/sudo docker-compose.yml stop
 - see https://github.com/ireceptorplus-inesctec/adc-middleware#java-properties-configuration-file
 - edit working working/config/middleware/deployment.properties at these entries <--!!!
```
 # REPLACE if using a different service name or path
adc.resourceServerUrl=https://repository1/airr/v1		<--!!!
app.resourceAllowedOrigins=*					<--!!!

server.address=0.0.0.0
server.port=80

# Role of the user that's able to call /synchronize
app.synchronizeRole=synchronize

# UMA Keycloak
uma.wellKnownUrl=https://server1/auth/realms/master/.well-known/uma2-configuration  <--!!!
uma.adminUrl=https://server1/auth/admin/realms/master				    <--!!!
# REPLACE if using a different client name
uma.clientId=adc-middleware
uma.clientSecret=vyXtgLBOKDGP7cXFrrqQtC2bHK23gvJi				<--!!!
# REPLACE with keycloak owner's ID
uma.resourceOwner=owner

# Keycloak Extenstion's URL
uma.keycloakExtensionApiUri=https://server1/kclext				<--!!!

```

## 10) acl-dashboard
 - git clone https://github.com/ireceptorplus-inesctec/acl-dashboard.git to /var/data
 - cd acl-dashboard 
 - if you build your own image vim .env
 
```
 PUBLIC_PATH=/
VUE_APP_KEYCLOAK_URL=https://server1/auth/
VUE_APP_KEYCLOAK_REALM=master
VUE_APP_KEYCLOAK_CLIENT_ID=acl-dashboard
VUE_APP_MIDDLEWARE_URL=http://server/
VUE_APP_MAPPINGS_BASE_PATH=resource/
VUE_APP_AUTHZ_BASE_PATH=authz/
NODE_ENV=production 
```
 - if you download the image edit docker-compose.yml
```
 services:
     acl-dashboard:
     	image: ...
     	volumes:
     		  - ./config:/config	<--!!!
     	environment:
           - PUBLIC_PATH=/acl-dashboard/ 			<--!!! and below
           - VUE_APP_KEYCLOAK_URL=https://server1/auth/
           - VUE_APP_KEYCLOAK_REALM=master
           - VUE_APP_KEYCLOAK_CLIENT_ID=acl-dashboard
           - VUE_APP_MIDDLEWARE_URL=https://server1/
	ports: up
	  - 9091:80 <--!!!
     	
     ..

  
```
- mv .env.docker _env.docker
- start container with sudo docker-compose up


## 11) update all containers with custom certificate files
 - copy files ca.crt, intermediate.crt to /var/data/adc-middleware/working/config
 - verify chmod 644 and name given to files in podman-adcauth.conf.sh var GLOBAL_CUSTOM_CERTIFICATES_LIST=( "intermediate.cert.pem.crt" "ca.cert.pem.crt")
 - if neceassary rename accordingly, otherwise no communication with nginx is possible
 - cd /var/data/scripts and check correct container names 
 vim mw-update-certs.sh
 	```
	..
	DCKCNTNAME_MIDDLEWARE="working_middleware_1"
	DCKCNTNAME_ACLDASH="acl-dashboard-27_acl-dashboard_1"
	DCKCNTNAME_KEYCLOAKEXTAPI="working_keycloakextensionapi_1"
	..
 	```
 
  - check: middleware and acl-dashboard containers running
  - sudo ./mw-update-certs.sh 
 

## 12) synchronize test study data "PRJNA330606" in adc repository with keycloak/ middleware
 - use curl with correct client_secret
 - get an access token as user owner 
```
curl https://server1/auth/realms/master/protocol/openid-connect/token \
-H "Content-Type: application/x-www-form-urlencoded" \
-d "grant_type=password&username=owner&password=owner&client_id=adc-middleware&client_secret=vyXtgLBOKDGP7cXFrrqQtC2bHK23gvJi" | jq
```

- get the access token value from the previous call and add it to the synchronize call
- owner can be specified by keycloak id or email
 
```
curl --location --request POST 'https://server1/airr/v1/synchronize' -H "Content-Type: application/json" --header 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ3VmJOLUNRSG00cDBtS1lsUWZ2blB5WUtuQ2xBQXV3QUhleENLT0J0OEFBIn0.eyJleHAiOjE2NjAyMDUzNzcsImlhdCI6MTY2MDIwNTMxNywianRpIjoiN2FhMzc0OGItZGQ0MS00ODk1LTlkZDUtZGQyN2I0ZTUzN2UzIiwiaXNzIjoiaHR0cHM6Ly9zZXJ2ZXIxL2F1dGgvcmVhbG1zL21hc3RlciIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiI1NzkzYjNjMy1kZWU1LTRjNGItYWY1MS1hYzllMzI1MmJhYTMiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJhZGMtbWlkZGxld2FyZSIsInNlc3Npb25fc3RhdGUiOiI3ZjE3NWViOC0yYjMzLTRhZmMtODMxZi04YzVkODQxOGNlYjQiLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImRlZmF1bHQtcm9sZXMtbWFzdGVyIiwib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiIsInN5bmNocm9uaXplIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwic2lkIjoiN2YxNzVlYjgtMmIzMy00YWZjLTgzMWYtOGM1ZDg0MThjZWI0IiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJuYW1lIjoiSm9obiBPd25lciIsInByZWZlcnJlZF91c2VybmFtZSI6Im93bmVyIiwiZ2l2ZW5fbmFtZSI6IkpvaG4iLCJmYW1pbHlfbmFtZSI6Ik93bmVyIn0.SrpYSkYtoZMNgqiJwpVZTj8VdRZTTkAu0B_gewUSC7oWmgS2mM-rZX_yIPCtYnNKFuDgsRxeAz3Zp-RMwllKWUsQIDxsr0nNpybTAQ8-_ou0fjEPw6v7rx3c47uDqREopqtEE3RbsxFN49V3KCGcf9dWZd5k39_on8RKK6ZAmpV6PMqsh-ucQO5JYp8dn8naz-20OCcwZZCa5EaOboB1v9fPhzRLLsdFXWKw67_wnYsLGaBSlZVFzR8WTP7vL-5qHHT5-RqfIkMl0Nk3GybhiNoCII8pMW9Eqh0LwZc7bKW-D2ipFSjl34qf4aURQNtrW6qW9p_0xM4h6tjo6EwBig' \
--data '[{ "owner": "5793b3c3-dee5-4c4b-af51-ac9e3252baa3", "studies": ["PRJNA330606"] }]' | jq
```

- you should recieve status 200, then it was successfull and you can verify ownerhsip in keycloak:
clients - adc-middleware - Authorization - Resources: PRJNA330606 is assigned to owner
- also verify in acl-dashboard: sign in as owner - My resources should list study PRJNA330606
```
{
  "status": 200
}
```

## 13) Share the resource with owner
 - access https://server1/acl-dashboard/resources
 - share all scopes with owner
 - F12 developer console shows 3 requests to https://server1/authz/give_access/null
 	with Form data like this:
 	e.g. resource_id=ed84d4fd-e726-49e3-b2dd-52859c1a2505&requester_id=owner&scope_name=statistics
 - This requires communication between client and middleware and middleware and keycloak
 
 
## 14) access protected ressource
 
 - a) get access token for owner
```
 curl https://server1/auth/realms/master/protocol/openid-connect/token \
-H "Content-Type: application/x-www-form-urlencoded" \
-d "grant_type=password&username=owner&password=owner&client_id=adc-middleware&client_secret=vyXtgLBOKDGP7cXFrrqQtC2bHK23gvJi" | jq
```
 
- b) get permission ticket for repertoire PRJNA330606-268 of test study
```
curl --location --request GET https://server1/airr/v1/repertoire/PRJNA330606-268 -H "Content-Protected: true" --head
```

- c) get RPT token with bearer token and permission ticket from previous calls

```
curl --location --request POST 'https://server1/auth/realms/master/protocol/openid-connect/token' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--header 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ3VmJOLUNRSG00cDBtS1lsUWZ2blB5WUtuQ2xBQXV3QUhleENLT0J0OEFBIn0.eyJleHAiOjE2NjAyMjQ0NjEsImlhdCI6MTY2MDIyNDQwMSwianRpIjoiZmUxY2Q1ZTYtYzFhZi00ODYyLTlkNjMtYTJkZTgyMDdiMGI0IiwiaXNzIjoiaHR0cHM6Ly9zZXJ2ZXIxL2F1dGgvcmVhbG1zL21hc3RlciIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiI1NzkzYjNjMy1kZWU1LTRjNGItYWY1MS1hYzllMzI1MmJhYTMiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJhZGMtbWlkZGxld2FyZSIsInNlc3Npb25fc3RhdGUiOiIwZjhhMTkzMy1kNjEwLTQ5ZjAtOGRiOC1mZmVmNDZjNjg4YTIiLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImRlZmF1bHQtcm9sZXMtbWFzdGVyIiwib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiIsInN5bmNocm9uaXplIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwic2lkIjoiMGY4YTE5MzMtZDYxMC00OWYwLThkYjgtZmZlZjQ2YzY4OGEyIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJuYW1lIjoiSm9obiBPd25lciIsInByZWZlcnJlZF91c2VybmFtZSI6Im93bmVyIiwiZ2l2ZW5fbmFtZSI6IkpvaG4iLCJmYW1pbHlfbmFtZSI6Ik93bmVyIn0.cuALagc7B4UVLmFZvu30vjaaJ-s1eG3Hsfor5-BQPhXq4vCXLSrdi40J9owWiDALgkp5ePgJwt8uExK6Q4NmkxyKI8eAk5y3baJJVfXvqRS8iAjp5lChRAJk3Uepb4K87Rt5Nr-8W3Im097g4-JF5oM5-XLys1lMor--58dyouOzqVE8NDK0J1bU77qVxflpSMADB5XVNT0hXXtvOyoJri5UgQFPrrRMqDNcDl7EwzsJzQcA140HqmhbEqXfaDfrdv2gD4f3P_j-CSXZWUypl_Zh913koJCxXqXZKMck9N_P39z4QTlBXQeoo_1aiB0B81fUjdULr8ADFPNlFni8_g' \
--data-urlencode 'grant_type=urn:ietf:params:oauth:grant-type:uma-ticket' \
--data-urlencode 'ticket=eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICI2MjFlNDU1My1kZjEzLTQwODMtYmY2YS1hMGZjNWQ5NjM3MjgifQ.eyJleHAiOjE2NjAyMjQ0NjYsIm5iZiI6MCwiaWF0IjoxNjYwMjI0NDA2LCJwZXJtaXNzaW9ucyI6W3sic2NvcGVzIjpbInB1YmxpYyIsInJhd19zZXF1ZW5jZSIsInN0YXRpc3RpY3MiXSwicnNpZCI6ImVkODRkNGZkLWU3MjYtNDllMy1iMmRkLTUyODU5YzFhMjUwNSJ9XSwianRpIjoiNWRiYjZjMzItOTk1Ni00MmIwLTg5MmItNzExYWExNjU1OTk5LTE2NjAyMjQ0MDY3ODMiLCJhdWQiOiJodHRwczovL3NlcnZlcjEvYXV0aC9yZWFsbXMvbWFzdGVyIiwic3ViIjoiNmFjMGY1ZGUtNjE0Ni00ZTUyLTg2MzMtODBkNWQ3ZTg0OThkIiwiYXpwIjoiYWRjLW1pZGRsZXdhcmUifQ.5bkcUTBnHYqQOH8FqY6FEIeV4pi0Qw04DBjKkyzNm8s' \ | jq

```

- d) get data with RPT token
```
curl --location --request \
GET 'https://server1/airr/v1/repertoire/PRJNA330606-268' \
-H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ3VmJOLUNRSG00cDBtS1lsUWZ2blB5WUtuQ2xBQXV3QUhleENLT0J0OEFBIn0.eyJleHAiOjE2NjAyMjQ1MDgsImlhdCI6MTY2MDIyNDQ0OCwianRpIjoiMjA0Y2JkNWEtMGU5MS00ZmRlLWI1MjMtYWJiZDNlNjhjMjk4IiwiaXNzIjoiaHR0cHM6Ly9zZXJ2ZXIxL2F1dGgvcmVhbG1zL21hc3RlciIsImF1ZCI6ImFkYy1taWRkbGV3YXJlIiwic3ViIjoiNTc5M2IzYzMtZGVlNS00YzRiLWFmNTEtYWM5ZTMyNTJiYWEzIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiYWRjLW1pZGRsZXdhcmUiLCJzZXNzaW9uX3N0YXRlIjoiMGY4YTE5MzMtZDYxMC00OWYwLThkYjgtZmZlZjQ2YzY4OGEyIiwiYWNyIjoiMSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJkZWZhdWx0LXJvbGVzLW1hc3RlciIsIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iLCJzeW5jaHJvbml6ZSJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sImF1dGhvcml6YXRpb24iOnsicGVybWlzc2lvbnMiOlt7InNjb3BlcyI6WyJwdWJsaWMiLCJyYXdfc2VxdWVuY2UiLCJzdGF0aXN0aWNzIl0sInJzaWQiOiJlZDg0ZDRmZC1lNzI2LTQ5ZTMtYjJkZC01Mjg1OWMxYTI1MDUiLCJyc25hbWUiOiJzdHVkeSBJRDogUFJKTkEzMzA2MDY7IHRpdGxlOiBUaGUgRGlmZmVyZW50IFQtY2VsbCBSZWNlcHRvciBSZXBlcnRvaXJlcyBpbiBCcmVhc3QgQ2FuY2VyIFR1bW9ycywgRHJhaW5pbmcgTHltcGggTm9kZXMsIGFuZCBBZGphY2VudCBUaXNzdWVzLiJ9XX0sInNjb3BlIjoiZW1haWwgcHJvZmlsZSIsInNpZCI6IjBmOGExOTMzLWQ2MTAtNDlmMC04ZGI4LWZmZWY0NmM2ODhhMiIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwibmFtZSI6IkpvaG4gT3duZXIiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJvd25lciIsImdpdmVuX25hbWUiOiJKb2huIiwiZmFtaWx5X25hbWUiOiJPd25lciJ9.ClpMiI3r7h4mZKDSD2MQUD9peivycPtmQAXSZATczuN96j8LmU5rCinWKwnaFII7reb2cgq2lXT8Rjtz-I6tS6UAJCPoCXu3DTfyoQcXnQOFQkdEFOAwouUIFeQBB3fz7PikRefFyybUGZEUuUJRxFWWXQYFh5BtLVcL26RPRSzmbiUH9bSshjFRU-PWYAs1XjVfCzlTqyIxm4GGcRY6HRgyTxN5DQKJVmURlpF5Sge_IqPe66vRELDcq_a-Ff4SLU9w9nwI9zx_QIM7LZT-y9xHVdtsI-uH5uFECuAMCQcewOuDmBckotmmxUFZdh_nCkVrW9yI5XRtGsAH1cTI4g' \
-H "Content-Protected: true" | jq
```

- verify data response

see file repertoire-data-PRJNA330606-268.json

## Integrate with ldap or identity providers
If you can see the protected repertoire data, all the components are working as expected across the ngingx service. Besides using local keycloak user accounts, you can also retrieve accounts from ldap database (like Microsoft Active Directory) or use a federated identity provider by OpenID Connect/OAuth 2.0.

## Container overview

Running containers, all software versions from 22-08-12

| IMAGE                                    | PORTS                                                                          | NAMES                                |
|------------------------------------------|--------------------------------------------------------------------------------|--------------------------------------|
| irpinesctec/adc-middleware:latest        | 0.0.0.0:9443->80/tcp, :::9443->80/tcp                                          | working_middleware_1                 |
| working_keycloak                         | 8443/tcp, 0.0.0.0:8081->8080/tcp, :::8081->8080/tcp                            | working_keycloak_1                   |
| irpinesctec/keycloak_extension_api:0.7.0 | 0.0.0.0:9000->5000/tcp, :::9000->5000/tcp                                      | working_keycloakextensionapi_1       |
| redis                                    | 6379/tcp                                                                       | working_middleware-redis_1           |
| postgres:12                              | 5432/tcp                                                                       | working_middleware-db_1              |
| postgres:12                              | 5432/tcp                                                                       | working_keycloak_db_1                |
| irpinesctec/acl-dashboard                | 0.0.0.0:9091->80/tcp, :::9091->80/tcp                                          | acl-dashboard-27_acl-dashboard_1     |
| ireceptor/service-php-mongodb:turnkey-v4 | 0.0.0.0:8180->80/tcp, :::8180->80/tcp, 0.0.0.0:8143->443/tcp, :::8143->443/tcp | turnkey-service_ireceptor-api_1      |
| ireceptor/repository-mongodb:turnkey-v4  | 27017/tcp                                                                      | turnkey-service_ireceptor-database_1 |

  
 
 

