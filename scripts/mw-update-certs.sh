#!/bin/bash
# install custom ca certificates into middleware containers

# -----------------------------------------------------------------------------
SCRIPT_DIR=`dirname "$0"`
#SCRIPT_DIR_FULL="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SCRIPT_DIR_FULL="$( readlink -f ${SCRIPT_DIR}  )";
POD_CONF_FILE="podman-adcauth.conf.sh" 

if [ ! -f "$SCRIPT_DIR/$POD_CONF_FILE" ]; then
    echo "The file $POD_CONF_FILE does not exist. Edit file podman-adcauth.conf.sh.EDIT then remove .EDIT"
    exit
fi

# read configuration for turnkey pod
. $SCRIPT_DIR_FULL/$POD_CONF_FILE 

# -----------------------------------------------------------------------------
DCKCNTNAME_MIDDLEWARE="working_middleware_1"
DCKCNTNAME_ACLDASH="acl-dashboard-27_acl-dashboard_1"
DCKCNTNAME_KEYCLOAKEXTAPI="working_keycloakextensionapi_1"
# -----------------------------------------------------------------------------

CERT_TARGET_PATH="/usr/local/share/ca-certificates"
if [ $GLOBAL_DO_UPDATE_CUSTOM_CERTIFICATES == "true" ]; then
    log "### install custom certificates to middleware container ..."
    for i in ${GLOBAL_CUSTOM_CERTIFICATES_LIST[@]}; do
        docker exec -d --user root $DCKCNTNAME_MIDDLEWARE sh -c "cd ${CERT_TARGET_PATH} && cp /config/${i} ${CERT_TARGET_PATH} && chmod 644 ${i}"
        sleep 1
        docker exec -d --user root $DCKCNTNAME_ACLDASH sh -c "cd ${CERT_TARGET_PATH} && cp /config/${i} ${CERT_TARGET_PATH} && chmod 644 ${i}"
        sleep 1
        docker exec -d --user root $DCKCNTNAME_KEYCLOAKEXTAPI sh -c "cd ${CERT_TARGET_PATH} && cp /config/${i} ${CERT_TARGET_PATH} && chmod 644 ${i}"
        sleep 1 && log "${i} installed to container os"
        docker exec -d --user root $DCKCNTNAME_MIDDLEWARE \
             sh -c "cd ${MIDWARE_KEYSTORE_PATH} && keytool -keystore cacerts -storepass changeit -noprompt -trustcacerts -importcert -alias cacert -file /config/${i}"
        sleep 1 && log "${i} installed to jdk truststore"
    done
    sleep 2
    docker exec -d --user root $DCKCNTNAME_MIDDLEWARE sh -c "cd ${CERT_TARGET_PATH} && /usr/sbin/update-ca-certificates"
    sleep 1
    docker exec -d --user root $DCKCNTNAME_ACLDASH sh -c "cd ${CERT_TARGET_PATH} && /usr/sbin/update-ca-certificates"
    sleep 1
    docker exec -d --user root $DCKCNTNAME_KEYCLOAKEXTAPI sh -c "cd ${CERT_TARGET_PATH} && /usr/sbin/update-ca-certificates"
    log "... done"
fi

#sleep 2
#cd /var/data/adc-middleware/working/
#docker-compose stop
#sleep 2
#docker-compose up &

#sleep 2
#cd /var/data/acl-dashboard
#docker-compose stop
#sleep 2
#docker-compose up &
echo "restart all containers now .. "
